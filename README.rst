clone_course
#############################

|pypi-badge| |ci-badge| |codecov-badge| |doc-badge| |pyversions-badge|
|license-badge| |status-badge|

Purpose
*******

Django application that provides a service for cloning courses in an Open edX instance.

The application provides a RESTful API that can be used to create a new course by cloning an existing one.
The application uses Celery to perform the cloning operation in the background,
and provides a task status endpoint to check the status of the task.


Getting Started
***************

Devstack Setup
==========
1. Add this package to requirements/edx/private.txt
2. Ensure that the Open edX platform devstack is running. Devstack won't run the Celery message broker by default.
   Ensure to start the redis broker by running the command ``make dev.up.redis``.
   (note: edx-platform should have the changes from `this commit`_)
.. _this commit: https://github.com/open-craft/edx-platform/commit/6dbef5a2478cc683bc17111024892edabf47b50e

Deploying
=========

If deploying with Tutor, add this package to OPENEDX_EXTRA_PIP_REQUIREMENTS, e.g.

``yaml
OPENEDX_EXTRA_PIP_REQUIREMENTS:
  - git+https://gitlab.com/opencraft/dev/clone_course
``

Usage
-----

API Endpoints
*************

**Clone course**

``POST STUDIO_URL/clone_course/clone/``

This API endpoint is used to clone an existing course to a new course.

The request body should contain a JSON object with the following fields:

- ``source_id``: The ID of the course to clone.
- ``dest_id``: The ID of the new course.

Additionally, you can provide the following optional fields to update the new course's settings:

- ``start_date``: The start date of the new course (required if any settings fields are provided).
- ``end_date``: The end date of the new course.
- ``enrollment_start``: The enrollment start date of the new course.
- ``enrollment_end``: The enrollment end date of the new course.
- ``overview``: The course overview.
- ``intro_video``: The course introduction video.
- ``effort``: The course effort.
- ``pre_requisite_courses``: A list of course IDs that are prerequisites for the new course.
- ``description``: The course description.
- ``short_description``: The course short description.
- ``course_image_asset_path``: The course image asset path.
- ``self_paced``: A boolean value indicating whether the course is self-paced.

Example Request:

.. code-block:: json

    {
        "source_id": "course-v1:edX+DemoX+Demo_Course",
        "dest_id": "course-v1:new+TestX+Demo_Course_Clone",
        "start_date": "2023-01-01T00:00:00Z",
        "overview": "This is a cloned course."
    }

Example Response:

.. code-block:: json

    {
        "task_id": "4f95e48a-8b68-45dc-942f-9ac19d5af352"
    }

**Check clone status**

``GET /api/v1/clone/status/<task_id>/``

This API endpoint is used to check the status of a cloning task.

The ``task_id`` should be the ID of the cloning task.

Example Request:

.. code-block:: http

    GET /api/v1/clone/status/4f95e48a-8b68-45dc-942f-9ac19d5af352/

Example Response:

.. code-block:: json

    {
        "status": "SUCCESS",
        "response":{"message":"Course cloned successfully."}
    }


Getting Started
***************

Developing
==========

One Time Setup
--------------
.. code-block::

  # Clone the repository
  git clone git@github.com:openedx/clone_course.git
  cd clone_course

  # Set up a virtualenv with the same name as the repo and activate it
  # Here's how you might do that if you have virtualenvwrapper setup.
  mkvirtualenv -p python3.8 clone_course


Every time you develop something in this repo
---------------------------------------------
.. code-block::

  # Activate the virtualenv
  # Here's how you might do that if you're using virtualenvwrapper.
  workon clone_course

  # Grab the latest code
  git checkout main
  git pull

  # Install/update the dev requirements
  make requirements

  # Run the tests and quality checks (to verify the status before you make any changes)
  make validate

  # Make a new branch for your changes
  git checkout -b <your_github_username>/<short_description>

  # Using your favorite editor, edit the code to make your change.
  vim ...

  # Run your new tests
  pytest ./path/to/new/tests

  # Run all the tests and quality checks
  make validate

  # Commit all your changes
  git commit ...
  git push

  # Open a PR and ask for review.

Deploying
=========

TODO: How can a new user go about deploying this component? Is it just a few
commands? Is there a larger how-to that should be linked here?

PLACEHOLDER: For details on how to deploy this component, see the `deployment how-to`_

.. _deployment how-to: https://docs.openedx.org/projects/clone_course/how-tos/how-to-deploy-this-component.html

Getting Help
************

Documentation
=============

PLACEHOLDER: Start by going through `the documentation`_.  If you need more help see below.

.. _the documentation: https://docs.openedx.org/projects/clone_course

(TODO: `Set up documentation <https://openedx.atlassian.net/wiki/spaces/DOC/pages/21627535/Publish+Documentation+on+Read+the+Docs>`_)

More Help
=========

If you're having trouble, we have discussion forums at
https://discuss.openedx.org where you can connect with others in the
community.

Our real-time conversations are on Slack. You can request a `Slack
invitation`_, then join our `community Slack workspace`_.

For anything non-trivial, the best path is to open an issue in this
repository with as many details about the issue you are facing as you
can provide.

https://github.com/openedx/clone_course/issues

For more information about these options, see the `Getting Help <https://openedx.org/getting-help>`__ page.

.. _Slack invitation: https://openedx.org/slack
.. _community Slack workspace: https://openedx.slack.com/

License
*******

The code in this repository is licensed under the AGPL 3.0 unless
otherwise noted.

Please see `LICENSE.txt <LICENSE.txt>`_ for details.

Contributing
************

Contributions are very welcome.
Please read `How To Contribute <https://openedx.org/r/how-to-contribute>`_ for details.

This project is currently accepting all types of contributions, bug fixes,
security fixes, maintenance work, or new features.  However, please make sure
to have a discussion about your new feature idea with the maintainers prior to
beginning development to maximize the chances of your change being accepted.
You can start a conversation by creating a new issue on this repo summarizing
your idea.

The Open edX Code of Conduct
****************************

All community members are expected to follow the `Open edX Code of Conduct`_.

.. _Open edX Code of Conduct: https://openedx.org/code-of-conduct/

People
******

The assigned maintainers for this component and other project details may be
found in `Backstage`_. Backstage pulls this data from the ``catalog-info.yaml``
file in this repo.

.. _Backstage: https://backstage.openedx.org/catalog/default/component/clone_course

Reporting Security Issues
*************************

Please do not report security issues in public. Please email security@openedx.org.

.. |pypi-badge| image:: https://img.shields.io/pypi/v/clone_course.svg
    :target: https://pypi.python.org/pypi/clone_course/
    :alt: PyPI

.. |ci-badge| image:: https://github.com/openedx/clone_course/workflows/Python%20CI/badge.svg?branch=main
    :target: https://github.com/openedx/clone_course/actions
    :alt: CI

.. |codecov-badge| image:: https://codecov.io/github/openedx/clone_course/coverage.svg?branch=main
    :target: https://codecov.io/github/openedx/clone_course?branch=main
    :alt: Codecov

.. |doc-badge| image:: https://readthedocs.org/projects/clone_course/badge/?version=latest
    :target: https://docs.openedx.org/projects/clone_course
    :alt: Documentation

.. |pyversions-badge| image:: https://img.shields.io/pypi/pyversions/clone_course.svg
    :target: https://pypi.python.org/pypi/clone_course/
    :alt: Supported Python versions

.. |license-badge| image:: https://img.shields.io/github/license/openedx/clone_course.svg
    :target: https://github.com/openedx/clone_course/blob/main/LICENSE.txt
    :alt: License

.. TODO: Choose one of the statuses below and remove the other status-badge lines.
.. |status-badge| image:: https://img.shields.io/badge/Status-Experimental-yellow
.. .. |status-badge| image:: https://img.shields.io/badge/Status-Maintained-brightgreen
.. .. |status-badge| image:: https://img.shields.io/badge/Status-Deprecated-orange
.. .. |status-badge| image:: https://img.shields.io/badge/Status-Unsupported-red
