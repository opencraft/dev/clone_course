.. clone_course documentation top level file, created by
   sphinx-quickstart on Tue Oct 24 10:35:00 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

clone_course
============

Provides endpoints for cloning courses.

Contents:

.. toctree::
   :maxdepth: 2

   readme
   getting_started
   quickstarts/index
   concepts/index
   how-tos/index
   testing
   internationalization
   modules
   changelog
   decisions
   references/index


Indices and tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
