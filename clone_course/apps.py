"""
clone_course Django application initialization.
"""

import logging

from django.apps import AppConfig
from edx_django_utils.plugins import PluginURLs

logger = logging.getLogger(__name__)


class CloneCourseConfig(AppConfig):
    """
    Configuration for the clone_course Django application.
    """

    name = 'clone_course'
    plugin_app = {
        PluginURLs.CONFIG: {
            'cms.djangoapp': {
                PluginURLs.NAMESPACE: name,
                PluginURLs.REGEX: '^clone_course/',
                PluginURLs.RELATIVE_PATH: 'urls',
            }
        }
    }

    def ready(self):
        """
        Emit a log when the app is loaded.
        """
        logger.info(f'{self.name} is ready.')
