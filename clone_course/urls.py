"""
URLs for clone_course.
"""
from django.urls import path

from .views import CloneCourse

urlpatterns = [
    path('clone/', CloneCourse.as_view({'post': 'clone'}), name='clone'),
    path('status/<task_id>/', CloneCourse.as_view({'get': 'status'}), name='status'),
]
