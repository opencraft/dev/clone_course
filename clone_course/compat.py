"""
Compatibility layer for testing without Open edX.
"""

from unittest.mock import Mock

from celery import Celery

# pylint: disable=unused-import

try:
    from cms import CELERY_APP
except ImportError:
    CELERY_APP = Celery(
        cache_backend="memory",
        result_backend="cache",
        task_always_eager=True,
        task_store_eager_result=True,
    )

try:
    from openedx.core.lib.api.authentication import OAuth2AuthenticationAllowInactiveUser
except ImportError:
    from rest_framework.authentication import SessionAuthentication as OAuth2AuthenticationAllowInactiveUser

try:
    from cms.djangoapps.api.v1.serializers.course_runs import CourseCloneSerializer
    from cms.djangoapps.contentstore.rest_api.v1.serializers import CourseDetailsSerializer
    from cms.djangoapps.contentstore.views.course import update_course_advanced_settings
    from cms.djangoapps.models.settings.course_metadata import CourseMetadata
    from common.djangoapps.student import auth
    from common.djangoapps.student.models.course_enrollment import CourseEnrollment
    from common.djangoapps.student.roles import CourseInstructorRole, CourseStaffRole
    from openedx.core.djangoapps.models.course_details import CourseDetails
    from openedx.core.lib.gating.api import (
        add_prerequisite,
        find_gating_milestones,
        get_prerequisites,
        set_required_content,
    )
    from xmodule.modulestore.django import modulestore
except ImportError:
    CourseCloneSerializer = Mock()
    CourseDetailsSerializer = Mock()
    update_course_advanced_settings = Mock()
    CourseMetadata = Mock()
    auth = Mock()
    CourseEnrollment = Mock()
    CourseInstructorRole = Mock()
    CourseStaffRole = Mock()
    CourseDetails = Mock()
    add_prerequisite = Mock()
    find_gating_milestones = Mock(return_value=[])
    get_prerequisites = Mock(return_value=[])
    set_required_content = Mock()
    modulestore = Mock()
