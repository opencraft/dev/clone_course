""" Tests for tasks. """

from unittest.mock import Mock, patch

from django.contrib.auth import get_user_model
from django.test import TestCase
from opaque_keys.edx.keys import CourseKey
from rest_framework import exceptions

from ..tasks import (
    _remove_instructor_enrollment,
    clone_course_task,
    send_clone_course_result_signal,
    update_advanced_settings,
    update_course_details,
)

User = get_user_model()


class TestCloneCourseTasks(TestCase):
    """Tests for clone_course.tasks."""

    dest_id = "course-v1:edx+DemoX+2023_clone"
    source_id = "course-v1:edx+DemoX+2023"
    expected = {"source_id": source_id, "dest_id": dest_id, "status": "SUCCESS"}
    failed = {
        "source_id": source_id,
        "dest_id": dest_id,
        "status": "ERROR",
        "error": "Failed something",
    }

    def setUp(self):
        super().setUp()
        self.user = User.objects.create(
            username="test-user",
            email="test-user@example.com",
        )

    @patch("clone_course.tasks.CourseDetailsSerializer")
    @patch("clone_course.tasks._remove_instructor_enrollment")
    @patch("clone_course.tasks.CourseCloneSerializer")
    def test_clone_course_success(
        self, mock_clone: Mock, mock_instructor: Mock, mock_details: Mock
    ):
        mock_details_instance = Mock()
        mock_details_instance.data = {}
        mock_details.return_value = mock_details_instance
        mock_clone_instance = Mock()
        mock_clone.return_value = mock_clone_instance

        result = clone_course_task(
            source_id=self.source_id, dest_id=self.dest_id, user_id=self.user.id
        )
        mock_clone.assert_called()
        mock_clone_instance.is_valid.assert_called()
        mock_clone_instance.save.assert_called()

        mock_instructor.assert_called_once_with(self.dest_id, self.user.id)

        self.assertEqual(result, self.expected)

    @patch("clone_course.tasks.CourseDetailsSerializer")
    @patch("clone_course.tasks.CourseCloneSerializer")
    def test_clone_course_failed_validation(self, mock_clone: Mock, mock_details: Mock):
        mock_details_instance = Mock()
        mock_details_instance.data = {}
        mock_details.return_value = mock_details_instance
        mock_clone_instance = Mock()
        mock_clone_instance.is_valid.side_effect = exceptions.ValidationError()
        mock_clone_instance.errors = "Validation error"
        mock_clone.return_value = mock_clone_instance

        result = clone_course_task(
            source_id=self.source_id, dest_id=self.dest_id, user_id=self.user.id
        )
        mock_clone.assert_called()
        mock_clone_instance.is_valid.assert_called()
        mock_clone_instance.save.assert_not_called()

        self.assertEqual(result, {**self.failed, "error": "Validation error"})

    @patch("clone_course.tasks.CourseDetailsSerializer")
    @patch("clone_course.tasks.CourseCloneSerializer")
    def test_clone_course_failed_clone(self, mock_clone: Mock, mock_details: Mock):
        mock_details_instance = Mock()
        mock_details_instance.data = {}
        mock_details.return_value = mock_details_instance
        mock_clone_instance = Mock()
        mock_clone_instance.save.side_effect = Exception("Failed to clone the course")
        mock_clone.return_value = mock_clone_instance

        result = clone_course_task(
            source_id=self.source_id, dest_id=self.dest_id, user_id=self.user.id
        )
        mock_clone.assert_called()
        mock_clone_instance.is_valid.assert_called()
        mock_clone_instance.save.assert_called()

        self.assertEqual(result, {**self.failed, "error": "Failed to clone the course"})

    @patch("clone_course.tasks.update_course_advanced_settings")
    def test_update_advanced_settings(self, mock_update: Mock):
        result = update_advanced_settings(self.expected, self.dest_id, {}, self.user.id)
        mock_update.assert_called()
        self.assertEqual(result, self.expected)

    @patch("clone_course.tasks.update_course_advanced_settings")
    def test_update_advanced_settings_skip(self, mock_update: Mock):
        result = update_advanced_settings(self.failed, self.dest_id, {}, self.user.id)
        mock_update.assert_not_called()
        self.assertEqual(result, self.failed)

    @patch("clone_course.tasks.CourseDetails")
    def test_update_course_details(self, mock_details: Mock):
        result = update_course_details(self.expected, self.dest_id, {}, self.user.id)
        mock_details.update_from_json.assert_called()
        self.assertEqual(result, self.expected)

    @patch("clone_course.tasks.CourseDetails")
    def test_update_course_details_skip(self, mock_details: Mock):
        result = update_course_details(self.failed, self.dest_id, {}, self.user.id)
        mock_details.update_from_json.assert_not_called()
        self.assertEqual(result, self.failed)


@patch("clone_course.tasks.get_user")
@patch("clone_course.tasks.auth")
@patch("clone_course.tasks.CourseStaffRole")
@patch("clone_course.tasks.CourseInstructorRole")
def test_remove_instructor(mock_instructor, mock_staff, mock_auth, mock_user):
    """Tests for _remove_instructor."""

    user_id = 42
    user = {"id": user_id}
    mock_user.return_value = user

    mock_instance = Mock()
    mock_instructor.return_value = mock_instance

    course_id = "course-v1:edx+DemoX+2023_clone"
    _remove_instructor_enrollment(course_id, user_id)

    mock_user.assert_called_with(user_id)

    mock_instructor.assert_called_with(CourseKey.from_string(course_id))
    mock_instance.remove_users.assert_called_with({"id": user_id})

    mock_auth.remove_users.assert_called_with(user, mock_staff(), user)


@patch("clone_course.tasks.COURSE_CLONE_TASKS_COMPLETED")
def test_clone_course_result_signal(mock_signal):
    """Tests for clone_course_result_signal."""

    send_clone_course_result_signal(
        result={
            "status": "SUCCESS",
        },
        source_id="course-v1:edx+DemoX+2023",
        dest_id="course-v1:edx+DemoX+2023_clone",
        user_id=42,
    )

    mock_signal.send.assert_called_with(
        sender=None,
        source_id="course-v1:edx+DemoX+2023",
        dest_id="course-v1:edx+DemoX+2023_clone",
        user_id=42,
        status="SUCCESS",
        error="",
    )
