""" Tests for views. """
from unittest.mock import MagicMock, Mock, patch

from celery import exceptions
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient

User = get_user_model()


class TestCloneCourseViews(TestCase):
    """ Tests for clone_course.views. """

    dest_id = 'course-v1:edx+DemoX+2023_clone'
    source_id = 'course-v1:edx+DemoX+2023'
    request = {
        'source_id': source_id,
        'dest_id': dest_id
    }
    expected = {
        **request,
        'status': 'SUCCESS'
    }
    failed = {
        **request,
        'status': 'ERROR'
    }
    error = {
        **failed,
        'error': 'Failed something'
    }

    def setUp(self):
        super().setUp()
        self.user = User.objects.create(
            username='test-user',
            email='test-user@example.com',
            password='123'
        )
        self.admin = User.objects.create(
            username='admin-user',
            email='admin-user@example.com',
            password='admin123',
            is_staff=True
        )

    def test_clone_unauthenticated(self):
        client = APIClient()
        response = client.post(reverse('clone'), self.request)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')

    def test_clone_nonadmin(self):
        client = APIClient()
        client.force_authenticate(user=self.user)
        response = client.post(reverse('clone'), self.request)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json()['detail'], 'You do not have permission to perform this action.')

    @patch('clone_course.tasks.CourseDetailsSerializer')
    def test_clone(self, mock_details: Mock):
        mock_details_instance = Mock()
        mock_details_instance.data = {}
        mock_details.return_value = mock_details_instance

        client = APIClient()
        client.force_authenticate(user=self.admin)
        data = {
            **self.request,
            'invitation_only': 'true',
            'start_date': '2023-10-31T00:00'
        }
        response = client.post(reverse('clone'), data)
        self.assertEqual(response.status_code, 201)
        self.assertIn('task_id', response.json())

    def test_status_unauthenticated(self):
        client = APIClient()
        response = client.get(reverse('status', kwargs={'task_id': '123'}))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')

    def test_status_nonadmin(self):
        client = APIClient()
        client.force_authenticate(user=self.user)
        response = client.get(reverse('status', kwargs={'task_id': '123'}))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json()['detail'], 'You do not have permission to perform this action.')

    def test_status_notask(self):
        client = APIClient()
        client.force_authenticate(user=self.admin)
        response = client.get(reverse('status', kwargs={'task_id': '123'}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['task_status'], 'PENDING')
        self.assertNotIn('response', response.json())

    def test_status(self):
        client = APIClient()
        client.force_authenticate(user=self.admin)
        response = client.post(reverse('clone'), self.request)
        task_id = response.json()['task_id']
        response = client.get(reverse('status', kwargs={'task_id': task_id}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['task_status'], 'SUCCESS')
        self.assertEqual(response.json()['response'], self.expected)

    @patch('clone_course.views.AsyncResult.ready', MagicMock(return_value=False))
    @patch('clone_course.views.AsyncResult.get')
    def test_status_not_ready(self, mock_result: Mock):
        client = APIClient()
        client.force_authenticate(user=self.admin)
        response = client.post(reverse('clone'), self.request)
        task_id = response.json()['task_id']
        response = client.get(reverse('status', kwargs={'task_id': task_id}))
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('response', response.json())
        mock_result.assert_not_called()

    @patch('clone_course.tasks.update_course_advanced_settings', MagicMock())
    @patch('clone_course.views.AsyncResult.get', side_effect=exceptions.TimeoutError())
    def test_status_timeout(self, mock_result: Mock):
        client = APIClient()
        client.force_authenticate(user=self.admin)
        response = client.post(reverse('clone'), self.request)
        task_id = response.json()['task_id']
        response = client.get(reverse('status', kwargs={'task_id': task_id}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['task_status'], 'SUCCESS')
        self.assertEqual(response.json()['error'], 'Timed out waiting for task response')
        self.assertNotIn('response', response.json())
        mock_result.assert_called_once()
