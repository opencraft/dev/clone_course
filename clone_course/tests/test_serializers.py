""" Tests for serializers. """
from unittest.mock import Mock, patch

from ddt import data, ddt
from django.test import TestCase
from opaque_keys.edx.keys import CourseKey

from ..serializers import CloneCoursePrerequisitesSerializer, CloneCourseSerializer


@ddt
class TestCloneCourseSerializer(TestCase):
    """ Tests for clone_course.serializer. """

    dest_id = 'course-v1:edx+DemoX+2023_clone'
    invalid_key = 'edx/DemoX/2023'
    malformed_key = 'course-v1:edx+DemoX+2023+clone'
    source_id = 'course-v1:edx+DemoX+2023'
    valid_date = '2023-10-31T00:00'

    def test_valid_keys(self):
        attrs = {
            'source_id': self.source_id,
            'dest_id': self.dest_id
        }
        self.assertTrue(CloneCourseSerializer(data=attrs).is_valid())

    @data([source_id, invalid_key], [invalid_key, dest_id])
    def test_invalid_keys(self, value: list):
        attrs = {
            'source_id': value[0],
            'dest_id': value[1]
        }
        serializer = CloneCourseSerializer(data=attrs)
        self.assertFalse(serializer.is_valid())

    @data([source_id, malformed_key], [malformed_key, dest_id])
    def test_malformed_keys(self, value: list):
        attrs = {
            'source_id': value[0],
            'dest_id': value[1]
        }
        serializer = CloneCourseSerializer(data=attrs)
        self.assertFalse(serializer.is_valid())

    def test_same_keys(self):
        attrs = {
            'source_id': self.source_id,
            'dest_id': self.source_id
        }
        serializer = CloneCourseSerializer(data=attrs)
        self.assertFalse(serializer.is_valid())

    def test_missing_required_start_date(self):
        attrs = {
            'source_id': self.source_id,
            'dest_id': self.dest_id,
            'effort': '50h'
        }
        serializer = CloneCourseSerializer(data=attrs)
        self.assertFalse(serializer.is_valid())

    def test_required_start_date(self):
        attrs = {
            'source_id': self.source_id,
            'dest_id': self.dest_id,
            'effort': '50h',
            'start_date': self.valid_date
        }
        serializer = CloneCourseSerializer(data=attrs)
        self.assertTrue(serializer.is_valid())

    @data('start_date', 'end_date', 'enrollment_start', 'enrollment_end')
    def test_invalid_dates(self, field: str):
        attrs = {
            'source_id': self.source_id,
            'dest_id': self.dest_id,
            field: '2023-11-31'
        }
        serializer = CloneCourseSerializer(data=attrs)
        self.assertFalse(serializer.is_valid())

    def test_end_before_start(self):
        attrs = {
            'source_id': self.source_id,
            'dest_id': self.dest_id,
            'start_date': '2023-10-31T00:00',
            'end_date': '2023-10-30T00:00'
        }
        serializer = CloneCourseSerializer(data=attrs)
        self.assertFalse(serializer.is_valid())

    def test_enrollment_end_before_start(self):
        attrs = {
            'source_id': self.source_id,
            'dest_id': self.dest_id,
            'start_date': '2023-12-31T00:00',
            'enrollment_start': '2023-10-31T00:00',
            'enrollment_end': '2023-10-31T00:00'
        }
        serializer = CloneCourseSerializer(data=attrs)
        self.assertFalse(serializer.is_valid())

    def test_start_before_enrollment(self):
        attrs = {
            'source_id': self.source_id,
            'dest_id': self.dest_id,
            'start_date': '2023-10-31T00:00',
            'enrollment_start': '2023-12-31T00:00'
        }
        serializer = CloneCourseSerializer(data=attrs)
        self.assertFalse(serializer.is_valid())

    @data('both', 'about', 'none')
    def test_valid_catalog_visiblity(self, value: str):
        attrs = {
            'source_id': self.source_id,
            'dest_id': self.dest_id,
            'catalog_visibility': value
        }
        serializer = CloneCourseSerializer(data=attrs)
        self.assertTrue(serializer.is_valid(raise_exception=True))

    def test_invalid_catalog_visiblity(self):
        attrs = {
            'source_id': self.source_id,
            'dest_id': self.dest_id,
            'catalog_visibility': 'all'
        }
        serializer = CloneCourseSerializer(data=attrs)
        self.assertFalse(serializer.is_valid())

    def test_invalid_boolean(self):
        attrs = {
            'source_id': self.source_id,
            'dest_id': self.dest_id,
            'invitation_only': 'maybe'
        }
        serializer = CloneCourseSerializer(data=attrs)
        self.assertFalse(serializer.is_valid())

    @data('false', 'true', 'False', 'True', 'FALSE', 'TRUE')
    def test_valid_boolean(self, value):
        attrs = {
            'source_id': self.source_id,
            'dest_id': self.dest_id,
            'invitation_only': value
        }
        serializer = CloneCourseSerializer(data=attrs)
        self.assertTrue(serializer.is_valid(raise_exception=True))


@ddt
class TestCloneCoursePrerequisitesSerializer(TestCase):
    """ Tests for CloneCoursePrerequisitesSerializer. """

    def test_valid_input(self):
        attrs = {
            'source_course_id': 'course-v1:edx+DemoX+2023',
            'destination_course_id': 'course-v1:edx+DemoX+2024'
        }
        serializer = CloneCoursePrerequisitesSerializer(data=attrs)
        self.assertTrue(serializer.is_valid())

    @data(
        {'source_course_id': 'course-v1:edx+DemoX+2023', 'destination_course_id': 'course-v1:edx+DemoX+2023'},
        {'source_course_id': 'course-v1:edx+DemoX+2023', 'destination_course_id': 'course-v1:edx+DemoX+2023+clone'},
        {'source_course_id': 'course-v1:edx+DemoX+2023+source', 'destination_course_id': 'course-v1:edx+DemoX+2024'},
    )
    def test_invalid_input(self, attrs):
        serializer = CloneCoursePrerequisitesSerializer(data=attrs)
        self.assertFalse(serializer.is_valid())

    @patch('clone_course.serializers.set_required_content')
    @patch('clone_course.serializers.get_prerequisites')
    @patch('clone_course.serializers.find_gating_milestones')
    @patch('clone_course.serializers.add_prerequisite')
    def test_copy_prerequisites(
        self,
        mock_add_prerequisite: Mock,
        mock_find_prerequisite: Mock,
        mock_get_prerequisite: Mock,
        mock_set_required_content: Mock
    ):
        attrs = {
            'source_course_id': 'course-v1:edx+DemoX+2023',
            'destination_course_id': 'course-v1:edx+DemoX+2024'
        }

        mock_find_prerequisite.side_effect = _mock_find_gating_milestones
        mock_get_prerequisite.side_effect = _mock_get_prerequisites
        serializer = CloneCoursePrerequisitesSerializer(data=attrs)
        self.assertTrue(serializer.is_valid())
        prerequisites = serializer.create(serializer.validated_data)

        milestone_id = _mock_find_gating_milestones(attrs['destination_course_id'], 'fulfills')[0]['content_id']
        mock_add_prerequisite.assert_called_once_with(
            attrs['destination_course_id'],
            milestone_id
        )
        content_id = _mock_find_gating_milestones(attrs['destination_course_id'], 'requires')[0]['content_id']
        mock_set_required_content.assert_called_once_with(
            attrs['destination_course_id'],
            content_id,
            milestone_id,
            min_score='100',
            min_completion='100'
        )
        mock_get_prerequisite.assert_called_once_with(CourseKey.from_string(attrs['destination_course_id']))
        self.assertEqual(prerequisites, _mock_get_prerequisites(CourseKey.from_string(attrs['destination_course_id'])))


def _mock_get_prerequisites(course_key: CourseKey) -> list:
    """ Mock for get_prerequisites. """
    return [{
        'id': 1,
        'name': f'Gating milestone for block-v1:{course_key}+type@sequential+block@8d244cb470ee4966aa1700b7860f91d5',
    },]


def _mock_find_gating_milestones(course_id: str, relationship: str) -> list:
    """ Mock for find_gating_milestones. """
    if relationship == 'fulfills':
        return [{
            'id': 43,
            'name': f'Gating milestone for block-v1:{course_id}+type@sequential+block@8d244cb470ee4966aa1700b7860f91d5',
            'display_name': '',
            'namespace': f'block-v1:{course_id}+type@sequential+block@8d244cb470ee4966aa1700b7860f91d5.gating',
            'description': 'System defined milestone',
            'course_id': f'course-v1:{course_id}',
            'content_id': f'block-v1:{course_id}+type@sequential+block@8d244cb470ee4966aa1700b7860f91d5',
            'requirements': {}
        },]
    elif relationship == 'requires':
        return [{
            'id': 43,
            'name': f'Gating milestone for block-v1:{course_id}+type@sequential+block@8d244cb470ee4966aa1700b7860f91d5',
            'display_name': '',
            'namespace': f'block-v1:{course_id}+type@sequential+block@8d244cb470ee4966aa1700b7860f91d5.gating',
            'description': 'System defined milestone',
            'course_id': f'course-v1:{course_id}',
            'content_id': f'block-v1:{course_id}+type@sequential+block@afad7a5b2f184571a7832cbf987cc055',
            'requirements': {'min_score': '100', 'min_completion': '100'}
        },]
    return []
