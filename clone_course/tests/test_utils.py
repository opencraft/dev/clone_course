""" Tests for utils. """
from django.contrib.auth import get_user_model
from django.test import TestCase

from clone_course.utils import get_dummy_request

User = get_user_model()


class TestCloneCourseUtils(TestCase):
    """ Tests clone_course.utils. """

    dest_id = 'course-v1:edx+DemoX+2023_clone'
    source_id = 'course-v1:edx+DemoX+2023'

    def setUp(self):
        super().setUp()
        self.user = User.objects.create(
            username='test-user',
            email='test-user@example.com',
        )

    def test_get_dummy_request(self):
        dummy = get_dummy_request(self.user.id)
        self.assertEqual(dummy.user.id, self.user.id)
        self.assertEqual(dummy.user.email, self.user.email)

    def test_get_dummy_request_fails(self):
        self.assertRaises(User.DoesNotExist, get_dummy_request, user_id=42)
