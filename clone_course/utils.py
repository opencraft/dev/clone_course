"""
Utilities for Studio API.
"""

from django.contrib.auth import get_user_model
from django.http import HttpRequest

User = get_user_model()


def get_user(user_id: int):
    """
    Retrieve a user object based on the provided user ID.

    Args:
        user_id (int): The ID of the user to retrieve.

    Returns:
        User: The user object corresponding to the provided user ID.

    Raises:
        User.DoesNotExist: If no user with the provided ID exists.
    """
    return User.objects.get(id=user_id)


def get_dummy_request(user_id: int) -> HttpRequest:
    """
    Generate a Dummy request for Celery tasks.
    """

    class DummyRequest(HttpRequest):
        def __init__(self, user_id) -> None:
            super().__init__()
            self.user = get_user(user_id)

    return DummyRequest(user_id=user_id)
