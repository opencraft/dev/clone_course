"""
Constants for the api.
"""


ADVANCED_SETTINGS_KEYS = [
    'advanced_modules',
    'display_name',
    'catalog_visibility',
    'invitation_only'
]
