"""
Views for clone and status API endpoints.
"""
import logging

from celery import chain, exceptions
from celery.result import AsyncResult
from rest_framework import permissions, viewsets
from rest_framework.response import Response

from .compat import OAuth2AuthenticationAllowInactiveUser
from .constants import ADVANCED_SETTINGS_KEYS
from .serializers import CloneCourseSerializer, StatusSerializer
from .tasks import (
    clone_course_task,
    send_clone_course_result_signal,
    send_clone_course_result_signal_error,
    update_advanced_settings,
    update_course_details,
)

logger = logging.getLogger(__name__)


class CloneCourse(viewsets.GenericViewSet):
    """
    Viewset for cloning courses.
    """

    authentication_classes = (OAuth2AuthenticationAllowInactiveUser,)
    permission_classes = (permissions.IsAdminUser,)

    def clone(self, request):
        """
        Call the celery task for cloning course and returns the task id.
        """
        serializer = CloneCourseSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        source_id = serializer.validated_data['source_id']
        dest_id = serializer.validated_data['dest_id']

        new_course_fields = {}
        advanced_settings = {}
        for k, v in serializer.validated_data.items():
            if k not in ('source_id', 'dest_id'):
                if k in ADVANCED_SETTINGS_KEYS:
                    advanced_settings[k] = v
                else:
                    new_course_fields[k] = v

        # Create a list of tasks to chain
        user_id = request.user.id
        tasks_list = [
            clone_course_task.s(source_id=source_id, dest_id=dest_id, user_id=user_id)
        ]

        if new_course_fields:
            tasks_list.append(update_course_details.s(
                course_id=dest_id, course_settings=new_course_fields, user_id=user_id))

        # Patch the advanced settings even if no values provided with the clone course request
        tasks_list.append(update_advanced_settings.s(
            course_id=dest_id, advanced_settings=advanced_settings, user_id=user_id))

        tasks_list.append(send_clone_course_result_signal.s(source_id=source_id, dest_id=dest_id, user_id=user_id))

        # Chain the tasks and apply_async
        tasks_chain = chain(*tasks_list)
        result = tasks_chain.apply_async(
            link_error=send_clone_course_result_signal_error.s(
                source_id=source_id,
                dest_id=dest_id,
                user_id=user_id,
            )
        )
        task_id = result.task_id
        return Response({"task_id": task_id}, status=201)

    def status(self, request, task_id):
        """
        Query the task status.
        """
        serializer = StatusSerializer(data={'task_id': task_id})
        serializer.is_valid(raise_exception=True)

        result = AsyncResult(serializer.validated_data['task_id'])
        response = {
            "task_id": result.id,
            "task_status": result.status,
        }

        # Check if ready so result.get don't block
        if result.ready():
            try:
                response["response"] = result.get(timeout=1.)
            except exceptions.TimeoutError:
                response["error"] = "Timed out waiting for task response"
            except Exception as error:
                logging.error(error, exc_info=True)
                response["error"] = str(error)

        return Response(response)
