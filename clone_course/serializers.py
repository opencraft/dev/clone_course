"""
Serializers for input parameters.
"""
from opaque_keys import InvalidKeyError
from opaque_keys.edx.keys import CourseKey
from rest_framework import serializers

from .compat import add_prerequisite, find_gating_milestones, get_prerequisites, set_required_content
from .constants import ADVANCED_SETTINGS_KEYS


class OptionalBooleanField(serializers.CharField):
    """
    Custom serializer for boolean value fields.
    """

    def __init__(self, field_name=None, **kwargs):
        """
        Initialize the class.
        """
        self.field_name = field_name
        super().__init__(**kwargs)

    def to_internal_value(self, data):
        """
        Return the respective boolean.
        """
        if isinstance(data, str) and data.lower() in ('true', 'false'):
            return data.lower() == 'true'
        raise serializers.ValidationError(
            f"Invalid value for {self.field_name}. It must be either 'true' or 'false'."
        )


class CloneCourseSerializer(serializers.Serializer):  # pylint: disable=abstract-method
    """
    Serializer for clone api.
    """

    source_id = serializers.CharField()
    dest_id = serializers.CharField()
    overview = serializers.CharField(required=False)
    intro_video = serializers.CharField(required=False)
    start_date = serializers.DateTimeField(required=False)
    end_date = serializers.DateTimeField(required=False)
    enrollment_start = serializers.DateTimeField(required=False)
    enrollment_end = serializers.DateTimeField(required=False)
    effort = serializers.CharField(required=False)
    pre_requisite_courses = serializers.ListField(required=False, child=serializers.CharField())
    description = serializers.CharField(required=False)
    short_description = serializers.CharField(required=False)
    course_image_asset_path = serializers.CharField(required=False)
    self_paced = OptionalBooleanField(field_name="self_paced", required=False)
    advanced_modules = serializers.ListField(required=False, child=serializers.CharField())
    display_name = serializers.CharField(required=False)
    catalog_visibility = serializers.CharField(required=False)
    invitation_only = OptionalBooleanField(field_name="invitation_only", required=False)

    def validate(self, attrs):
        """
        Validate input parameters.
        """
        if attrs['source_id'] == attrs['dest_id']:
            raise serializers.ValidationError(
                "Source course_id and Destination course_id cannot be the same"
            )
        if not (attrs['source_id'].startswith("course-v1:") or attrs['source_id'].startswith("library-v1:")):
            raise serializers.ValidationError(
                "Invalid source course id. Only course-v1/library-v1 prefixed keys are supported."
            )
        if not (attrs['dest_id'].startswith("course-v1:") or attrs['dest_id'].startswith("library-v1:")):
            raise serializers.ValidationError(
                "Invalid destination course id. Only course-v1/library-v1 prefixed keys are supported."
            )

        # Ensure course keys are well-formed
        try:
            CourseKey.from_string(attrs['source_id'])
        except InvalidKeyError:
            raise serializers.ValidationError('Malformed source course id')  # pylint: disable=raise-missing-from
        try:
            CourseKey.from_string(attrs['dest_id'])
        except InvalidKeyError:
            raise serializers.ValidationError('Malformed destination course id')  # pylint: disable=raise-missing-from

        # If any other fields other than source_id, dest_id and advanced settings are passed,
        # then start_date should be present.
        other_fields = set(attrs.keys()) - {'source_id', 'dest_id'} - set(ADVANCED_SETTINGS_KEYS)
        if other_fields:
            if 'start_date' not in attrs:
                raise serializers.ValidationError("The course must have an assigned start date.")

        # If end_date is passed, it can't be earlier than or same as start_date.
        if 'end_date' in attrs and 'start_date' in attrs:
            if attrs['end_date'] <= attrs['start_date']:
                raise serializers.ValidationError("The course end date must be later than the course start date.")

        # If enrollment_start is passed, it can't be later than start_date.
        if 'enrollment_start' in attrs and 'start_date' in attrs:
            if attrs['enrollment_start'] > attrs['start_date']:
                raise serializers.ValidationError("The course start date must be later than the enrollment start date.")

        # If enrollment_end is passed, it can't be earlier than or same as enrollment_start (if that's passed too).
        if 'enrollment_end' in attrs and 'enrollment_start' in attrs:
            if attrs['enrollment_end'] <= attrs['enrollment_start']:
                raise serializers.ValidationError("The enrollment start date cannot be after the enrollment end date")

        catalog_visibility_options = ["both", "about", "none"]
        if 'catalog_visibility' in attrs and attrs['catalog_visibility'] not in catalog_visibility_options:
            raise serializers.ValidationError(
                f"Invalid catalog_visibility value. Accepted values are: {', '.join(catalog_visibility_options)}"
            )

        return attrs


class StatusSerializer(serializers.Serializer):  # pylint: disable=abstract-method
    """
    Serializer for status api.
    """

    task_id = serializers.CharField()


class CloneCoursePrerequisitesSerializer(serializers.Serializer):  # pylint: disable=abstract-method
    """
    Serializer for cloning course prerequisites.
    """

    source_course_id = serializers.CharField()
    destination_course_id = serializers.CharField()

    def validate(self, attrs):
        """
        Validate input parameters.
        """
        if attrs['source_course_id'] == attrs['destination_course_id']:
            raise serializers.ValidationError(
                "Source course_id and Destination course_id cannot be the same"
            )
        if not attrs['source_course_id'].startswith("course-v1:"):
            raise serializers.ValidationError(
                "Invalid source course id. Only course-v1 prefixed keys are supported."
            )
        if not attrs['destination_course_id'].startswith("course-v1:"):
            raise serializers.ValidationError(
                "Invalid destination course id. Only course-v1 prefixed keys are supported."
            )

        # Ensure course keys are well-formed
        try:
            CourseKey.from_string(attrs['source_course_id'])
        except InvalidKeyError:
            raise serializers.ValidationError('Malformed source course id')  # pylint: disable=raise-missing-from
        try:
            CourseKey.from_string(attrs['destination_course_id'])
        except InvalidKeyError:
            raise serializers.ValidationError('Malformed destination course id')  # pylint: disable=raise-missing-from

        return attrs

    def create(self, validated_data):
        """
        Copy the prerequisites from the source course to the destination course.
        """
        source_course_id = validated_data.get('source_course_id')
        destination_course_id = validated_data.get('destination_course_id')

        source_course_key = CourseKey.from_string(source_course_id)
        destination_course_key = CourseKey.from_string(destination_course_id)

        src_ref = source_course_id.split(':')[-1]
        dest_ref = destination_course_id.split(':')[-1]
        cache = {}

        for prereq in find_gating_milestones(source_course_key, relationship='fulfills'):
            # Create the milestone
            src_content_id = prereq['content_id']
            dest_content_id = src_content_id.replace(src_ref, dest_ref)
            cache[prereq['namespace']] = dest_content_id
            add_prerequisite(destination_course_id, dest_content_id)

        for gated in find_gating_milestones(source_course_key, relationship='requires'):
            # Set the milestone as required for the content
            src_content_id = gated['content_id']
            dest_content_id = src_content_id.replace(src_ref, dest_ref)

            dest_milestone_id = cache[gated['namespace']]

            requirements = gated['requirements']
            set_required_content(destination_course_id, dest_content_id, dest_milestone_id, **requirements)

        return get_prerequisites(destination_course_key)
