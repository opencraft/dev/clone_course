"""Signals for the course cloning process."""
from django.dispatch import Signal

COURSE_CLONE_TASKS_COMPLETED = Signal()
