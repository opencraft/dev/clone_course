"""
Celery tasks to clone courses and modify settings.
"""

import logging

from opaque_keys.edx.keys import CourseKey
from rest_framework import exceptions

from .compat import CELERY_APP as app
from .compat import (
    CourseCloneSerializer,
    CourseDetails,
    CourseDetailsSerializer,
    CourseEnrollment,
    CourseInstructorRole,
    CourseMetadata,
    CourseStaffRole,
    auth,
    modulestore,
    update_course_advanced_settings,
)
from .constants import ADVANCED_SETTINGS_KEYS
from .serializers import CloneCoursePrerequisitesSerializer
from .signals import COURSE_CLONE_TASKS_COMPLETED
from .utils import get_dummy_request, get_user

logger = logging.getLogger(__name__)


def _remove_instructor_enrollment(course_id, user_id):
    """
    Remove an instructor from a course.

    Args:
        course_id (str): The ID of the course from which to remove the instructor.
        user_id (str): The ID of the instructor to be removed.

    Returns:
        None
    """
    course_key = CourseKey.from_string(course_id)
    user = get_user(user_id)
    CourseInstructorRole(course_key).remove_users(user)
    auth.remove_users(user, CourseStaffRole(course_key), user)
    CourseEnrollment.unenroll(user, course_key)


@app.task
def clone_course_task(source_id: str, dest_id: str, user_id: int) -> dict:
    """
    Celery task that clones a course.

    Args:
        source_id (str): The ID of the source course to be cloned.
        dest_id (str): The ID of the destination course where the cloned course will be created.
        user_id (int): The ID of the user performing the cloning operation.

    Returns:
        dict: A dictionary containing the status of the cloning operation.

    Raises:
        exceptions.ValidationError: If there is a validation error during the cloning process.
        Exception: If there is an error during the cloning process.

    """
    logger.info("Task started for calling studio api for cloning course")

    request = get_dummy_request(user_id=user_id)
    context = {"request": request}
    data = {"source_course_id": source_id, "destination_course_id": dest_id}
    result = {"source_id": source_id, "dest_id": dest_id, "status": "SUCCESS"}

    try:
        serializer = CourseCloneSerializer(data=data, context=context)
        serializer.is_valid(raise_exception=True)
        serializer.save()  # This clones the course
    except exceptions.ValidationError as e:
        logger.error(f"Validation error cloning course: {str(e)}")
        result["error"] = serializer.errors
        result["status"] = "ERROR"
    except Exception as e:
        logger.error(f"Error cloning course: {str(e)}")
        result["error"] = str(e)
        result["status"] = "ERROR"
    else:
        # Remove the instructor from the cloned course
        _remove_instructor_enrollment(dest_id, user_id)

        # Clone the prerequisites
        try:
            prereq_serializer = CloneCoursePrerequisitesSerializer(data=data)
            prereq_serializer.is_valid(raise_exception=True)
            prereq_serializer.save()
        except exceptions.ValidationError as e:
            logger.error(f"Validation error copying prerequisites: {str(e)}")
            result["error"] = prereq_serializer.errors
            result["status"] = "ERROR"
        except Exception as e:
            logger.error(f"Error copying prerequisites: {str(e)}")
            result["error"] = str(e)
            result["status"] = "ERROR"

    logger.info("Task completed for calling studio api for cloning course")
    return result


@app.task
def update_course_details(result, course_id: str, course_settings: dict, user_id: int):
    """
    Task to update course details.
    """
    # If the previous task failed, pass the result ahead.
    if "error" in result:
        return result

    logger.info("Task started for updating course settings")

    try:
        course_key = CourseKey.from_string(course_id)
        user = get_user(user_id)
        course_details = CourseDetails.fetch(course_key)
        serializer = CourseDetailsSerializer(course_details)
        existing_settings = serializer.data

        for field, value in course_settings.items():
            existing_settings[field] = value
        CourseDetails.update_from_json(course_key, existing_settings, user)
    except Exception as e:
        logger.error(f"Error updating course details: {str(e)}")
        result["error"] = str(e)
        result["status"] = "ERROR"

    logger.info("Task completed for updating course settings")
    return result


@app.task
def update_advanced_settings(
    result, course_id: str, advanced_settings: dict, user_id: int
):
    """
    Task to update advanced settings.
    """
    # If the previous task failed, pass the result ahead.
    if "error" in result:
        return result

    logger.info("Task started for updating course advanced settings")

    try:
        course_key = CourseKey.from_string(course_id)
        course_block = modulestore().get_course(course_key)

        # Patch advanced settings with existing values even if new
        # values are not provided with the clone course request.
        # This is done to trigger publish of the newly cloned course.
        course_metadata = CourseMetadata.fetch_all(course_block)
        new_settings = {
            key: {"value": course_metadata.get(key, {}).get("value")}
            for key in ADVANCED_SETTINGS_KEYS
        }
        for key in advanced_settings:
            new_settings[key] = {"value": advanced_settings.get(key)}

        user = get_user(user_id)
        update_course_advanced_settings(course_block, new_settings, user)
    except Exception as e:
        logger.error(f"Error updating course advanced settings: {str(e)}")
        result["error"] = str(e)
        result["status"] = "ERROR"

    logger.info("Task completed for updating course advanced settings")
    return result


@app.task
def send_clone_course_result_signal_error(_, exc, __, *args, source_id: str, dest_id: str, user_id: int):
    """
    Task to send signal to notify that the course cloning tasks failed.
    """
    send_clone_course_result_signal({"status": "ERROR", "error": str(exc)}, source_id, dest_id, user_id)


@app.task
def send_clone_course_result_signal(result, source_id: str, dest_id: str, user_id: int):
    """
    Task to send signal to notify that the course cloning tasks have been completed or failed.
    """
    status = result.get("status", "ERROR")
    error = result.get("error", "")
    COURSE_CLONE_TASKS_COMPLETED.send(
        sender=None, source_id=source_id, dest_id=dest_id, user_id=user_id, status=status, error=error
    )
    return result
